CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This creates a new entity reference field formatter to allow Create a new
entity, edit and delete referenced entity on view page of
entity that has the field.

  * For a full description of the module, visit the project page:
    https://drupal.org/project/entity_reference_crud_display

  * To submit bug reports and feature suggestions, or to track changes:
    https://drupal.org/project/issues/entity_reference_crud_display

REQUIREMENTS
------------

This module not require other modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

After installed, go to "Manage display" page of choose entity/node that has a
entity reference field, and choose the Format "Entity Reference CRUD Display".

MAINTAINERS
-----------

Current maintainers:

 * Bruno de Oliveira Magalhaes (bmagalhaes) - https://drupal.org/user/333078
 * Ronan Ribeiro (RonanRBR) - https://drupal.org/user/332925
 * Jonas Sousa (onaSousa) - https://drupal.org/user/3542330

This project has been sponsored by:

 * 7Links Web Solutions
   Specialized in consulting and planning of Drupal powered sites, 7Links
   offers installation, development, theming, customization, and hosting
   to get you started. Visit https://7links.com.br for more information.
